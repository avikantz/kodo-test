# Kodo Test App


### Stack and Libraries Used

-   [ReactJS](https://reactjs.org) Library
-   [Next.js](https://nextjs.org) Framework
-   [Typescript](https://www.typescriptlang.org)
-   [Cypress](https://www.cypress.io) Integration Tests
-   [Jest](https://jestjs.io) API and Unit tests



### Hosted version

A hosted version of the application can be found [here](https://kodo-test.vercel.app/).



### Running locally

-   Clone the project
-   Install [Node](https://nodejs.org/en/) (14+) on local system
-   `cd` into the project folder and run `npm install` to install packages
-   Run `npm run build` to build locally
-   Run `npm run start` to start the server
-   Visit `localhost:3000` on a browser to visit the application


### Testing

-   Run `npm run test` from the project directory to execute unit tests
-   Install [cypress](https://docs.cypress.io/guides/getting-started/installing-cypress#Direct-download) on local system
-   Load the project into Cypress or execute `npm run cypress` from the project directory to run integration tests


