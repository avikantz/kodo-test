/**
 * Searching utils
 */

export const strictSearch = (haystack: string, needle: string) => {
	const str = haystack.toLowerCase()
	const text = needle.toLowerCase()
	return str.includes(text)
} 

export const fuzzySearch = (haystack: string, needle: string) => {
	const str = haystack.toLowerCase()
	const text = needle.toLowerCase()
	let n = -1
	for (let i = 0; i < text.length; i += 1) {
		const l = text[i]
		const si = str.indexOf(l, n + 1)
		if (si == -1) {
			return false
		}
		n = si
	}
	return true
}
