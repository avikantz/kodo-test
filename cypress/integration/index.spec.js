/**
 * Home page
 */

/// <reference types="cypress" />

describe('Search', () => {
	beforeEach(() => {
		// Navigate to homepage
		cy.visit('/')
	})

	it('renders items', () => {
		// Check length of items rendered
		cy.get('div[class*="card"]').should('have.length', 6)
		//
		cy.get('tr').should('have.length', 101)
	})

	it('filters search results', () => {
		//
		cy.get('input').click()
		// Test filtering
		cy.get('input').type('lion king')
		// Check if filtering renders proper number of response
		cy.get('div[class*="card"]').should('have.length', 2)
		//
		cy.get('tr').should('have.length', 3)
	})
	
	it('sorts search results', () => {
		//
		cy.get('select').select('name')
		//
		cy.get('div[class*="card"]').first().should('contain.text', 'Central Creative Producer')
		//
		cy.get('select').select('dateLastEdited')
		//
		cy.get('div[class*="card"]').first().should('contain.text', 'Regional Marketing Developer')
	})
	
	it('paginates results', () => {
		//
		cy.get('input').click()
		// Test filtering
		cy.get('input').type('district assurance')
		// Check if filtering renders proper number of response
		cy.get('div[class*="card"]').should('have.length', 6)
		//
		cy.get('tr').should('have.length', 12)
		//
		cy.get('button[title="›"]').click()
		//
		cy.get('div[class*="card"]').should('have.length', 5)
	})
})
