/**
 * Colors
 */

export const COLORS = {
	BACKGROUND: '#ffffff',
	BORDER: '#f4f4f4',
	PRIMARY: '#0070f3',
	PRIMARY_LIGHT: '#0070f388',
	TEXT: '#343a40',
	SECONDARY: '#778c8d',
	DISABLED: 'rgba(255, 255, 255, 0.5)',
	SHADOW: 'rgba(0, 0, 0, 0.2)',
	SHADOW_DARK: 'rgba(0, 0, 0, 0.4)'
}
