/**
 * Page title
 */

import { FC } from 'react'

/// --------------------------------------------------------------------------------------------------------------------

export const Title: FC = ({ children }) => (
	<h1>
		{children}
		<style jsx>{`
			h1 a {
				border-width: 4px;
			}

			h1 {
				width: 100%;
				line-height: 1.15;
				font-size: 3rem;
			}

			@media (max-width: 479px) {
				h1 {
					font-size: 1.75rem;
				}
			}
		`}</style>
	</h1>
)
