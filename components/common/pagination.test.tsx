/**
 *
 */

import { render } from '@testing-library/react'

import { Pagination } from './pagination'

describe('Pagination', () => {
	it('is null with no items', () => {
		const pagination = render(<Pagination page={1} pageCount={5} />)
		expect(pagination.queryByRole('button')).toBeNull()
	})

	it('displays all pages for less than 5 pages', () => {
		const pageCount = Math.ceil(Math.random() * 5)
		const pagination = render(<Pagination page={1} pageCount={pageCount} totalCount={pageCount * 10} itemsPerPage={10} />)
		expect(pagination.queryAllByRole('button')).toHaveLength(2 + pageCount)
	})

	it('displays all pages for between 5 and 10 pages', () => {
		const pageCount = Math.ceil(Math.random() * 5 + 5)
		const pagination = render(<Pagination page={1} pageCount={pageCount} totalCount={pageCount * 10} itemsPerPage={10} />)
		expect(pagination.queryAllByRole('button')).toHaveLength(4 + pageCount)
	})

	it('displays trailing ellipses for pages > 10', () => {
		const pageCount = Math.ceil(Math.random() * 5 + 10)
		const pagination = render(<Pagination page={1} pageCount={pageCount} totalCount={pageCount * 10} itemsPerPage={10} />)
		expect(pagination.queryByText('⋯')).toBeDefined()
		const buttons = pagination.queryAllByRole('button')
		const index = buttons.findIndex(b => b.getAttribute('title') == '⋯')
		expect(index).toBe(7)
	})

	it('displays leading ellipses for pages > 10', () => {
		const pageCount = Math.ceil(Math.random() * 5 + 10)
		const pagination = render(<Pagination page={pageCount - 1} pageCount={pageCount} totalCount={pageCount * 10} itemsPerPage={10} />)
		expect(pagination.queryByText('⋯')).toBeDefined()
		const buttons = pagination.queryAllByRole('button')
		const index = buttons.findIndex(b => b.getAttribute('title') == '⋯')
		expect(index).toBe(2)
	})

	it('displays leading and trailing ellipses for pages > 10', () => {
		const pageCount = Math.ceil(Math.random() * 5 + 10)
		const pagination = render(<Pagination page={Math.floor(pageCount / 2)} pageCount={pageCount} totalCount={pageCount * 10} itemsPerPage={10} />)
		expect(pagination.queryAllByText('⋯')).toHaveLength(2)
		const buttons = pagination.queryAllByRole('button')
		expect(buttons).toHaveLength(11)
		expect(buttons[2].getAttribute('title')).toBe('⋯')
		expect(buttons[8].getAttribute('title')).toBe('⋯')
	})
})
