/**
 * Search bar
 */

import { ChangeEventHandler, FC } from 'react'

import { COLORS } from 'styles'

/// --------------------------------------------------------------------------------------------------------------------

interface SearchInputProps {
	value?: string
	onChange?: ChangeEventHandler<HTMLInputElement>
}

/// --------------------------------------------------------------------------------------------------------------------

export const SearchInput: FC<SearchInputProps> = ({ value, onChange }) => (
	<>
		<input type="search" placeholder="E.g. customer" value={value} onChange={onChange} />
		<style jsx>{`
			padding: 0.25rem 0.5rem;
			font-size: 1rem;
			border: 1px solid ${COLORS.BORDER};
			border-radius: 8px;
		`}</style>
	</>
)
