/**
 * Pagination
 */

import { FC } from 'react'

import { COLORS } from 'styles'

/// --------------------------------------------------------------------------------------------------------------------

interface PageButtonProps {
	title: string
	first?: boolean
	last?: boolean
	selected?: boolean
	disabled?: boolean
	onClick?: any
}

interface PaginationProps {
	page: number // Current active page
	pageCount: number // Total number of pages
	totalCount?: number // Total number of items
	itemsPerPage?: number // Number of items on a page
	showStats?: boolean
	onChange?: any // Triggered on page change
}

/// --------------------------------------------------------------------------------------------------------------------

const PageButton: FC<PageButtonProps> = ({ title, selected = false, disabled = false, onClick }) => (
	<button title={title} disabled={disabled} onClick={!disabled && !selected && onClick ? onClick : null}>
		{title}

		<style jsx>{`
			display: inline-block;
			padding: 0.5rem 0.75rem;
			margin: 0;
			font-weight: bold;
			border-top-width: 1px;
			border-left-width: 1px;
			border-bottom-width: 1px;
			border-right-width: 0;
			border-style: solid;
			border-color: ${COLORS.BORDER};
			color: ${selected ? 'white' : COLORS.PRIMARY};
			background: ${selected ? COLORS.PRIMARY : 'white'};

			& :first-child {
				border-radius: 8px 0 0 8px;
			}
			& :last-child {
				border-right-width: 1px;
				border-radius: 0 8px 8px 0;
			}

			& :disabled {
				background: ${COLORS.SHADOW};
				color: ${COLORS.DISABLED};
			}
			& :disabled:hover {
				background: ${COLORS.SHADOW};
				color: ${COLORS.DISABLED}
				cursor: not-allowed;
			}

			& :hover {
				background: ${COLORS.PRIMARY_LIGHT};
				color: ${COLORS.BACKGROUND};
				cursor: pointer;
			}
		`}</style>
	</button>
)

/// --------------------------------------------------------------------------------------------------------------------

export const Pagination: FC<PaginationProps> = ({ page = 1, pageCount = 1, totalCount = 0, itemsPerPage = 20, showStats = false, onChange }) => {
	//
	if (pageCount == 0 || totalCount == 0) return null
	//
	const onPageChange = (page = 1) => {
		onChange && onChange(page)
	}
	//
	return (
		<div className="pagination-container">
			{/** Row of buttons */}
			<div className="pagination">
				{/** First page */}
				{pageCount > 5 && <PageButton title="«" disabled={page == 1} onClick={() => onPageChange(1)} />}
				{/** Go back one */}
				<PageButton title="‹" disabled={page == 1} onClick={() => onPageChange(page - 1)} />
				{/** Display all case (<10 pages) */}
				{pageCount <= 10 &&
					Array(pageCount)
						.fill(0)
						.map((item, index) => (
							<PageButton
								key={'page' + (index + 1)}
								title={'' + (index + 1)}
								disabled={page == index + 1}
								onClick={() => onPageChange(index + 1)}
							/>
						))}
				{/** Start of a large list */}
				{pageCount > 10 && page < 5 && (
					<>
						{Array(5)
							.fill(0)
							.map((item, index) => (
								<PageButton
									key={'page' + (index + 1)}
									title={'' + (index + 1)}
									selected={page == index + 1}
									onClick={() => onPageChange(index + 1)}
								/>
							))}
						<PageButton title="⋯" />
					</>
				)}
				{/** Middle of a large list */}
				{pageCount > 10 && page >= 5 && page <= pageCount - 4 && (
					<>
						<PageButton title="⋯" />
						{Array(5)
							.fill(0)
							.map((item, index) => (
								<PageButton
									key={'page' + (page + index - 2)}
									title={'' + (page + index - 2)}
									selected={index == 2}
									onClick={() => onPageChange(page + index - 2)}
								/>
							))}
						<PageButton title="⋯" />
					</>
				)}
				{/** End of large list */}
				{pageCount > 10 && page > pageCount - 4 && (
					<>
						<PageButton title="⋯" />
						{Array(5)
							.fill(0)
							.map((item, index) => (
								<PageButton
									key={'page' + (pageCount + index - 4)}
									title={'' + (pageCount + index - 4)}
									selected={page == pageCount + index - 4}
									onClick={() => onPageChange(pageCount + index - 4)}
								/>
							))}
					</>
				)}
				{/** Go forward one */}
				<PageButton title="›" disabled={page == pageCount} onClick={() => onPageChange(page + 1)} />
				{/** Go to last */}
				{pageCount > 5 && <PageButton title="»" disabled={page == pageCount} onClick={() => onPageChange(pageCount)} />}
			</div>
			{/** Stats */}
			{showStats && totalCount > 0 && (
				<small className="info">
					Displaying items <strong>{(page - 1) * itemsPerPage + 1}</strong> to <strong>{Math.min(totalCount, page * itemsPerPage)}</strong> of{' '}
					<strong>{totalCount}</strong>
				</small>
			)}

			<style jsx>{`
				.pagination-container {
					display: block;
					width: 100%;
					white-space: nowrap;
					overflow-x: scroll;
				}

				.pagination {
					text-align: right;
					display: block;
					margin-bottom: 0.5rem;
				}

				.pagination button:first-child {
					border-radius: 8px 0 0 8px;
				}
				.pagination button:last-child {
					border-right-width: 1px;
					border-radius: 0 8px 8px 0;
				}

				.info {
					display: block;
					text-align: right;
					font-size: 0.75rem;
					color: ${COLORS.SECONDARY};
				}
			`}</style>
		</div>
	)
}
