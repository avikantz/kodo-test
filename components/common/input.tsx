/**
 * Input containers
 */

import { FC } from 'react'

/// --------------------------------------------------------------------------------------------------------------------

interface InputItemProps {
	label: string
}

/// --------------------------------------------------------------------------------------------------------------------

export const InputItem: FC<InputItemProps> = ({ label, children }) => (
	<div>
		<label>{label}</label>
		{children}

		<style jsx>{`
			display: flex;
			flex-direction: column;
			gap: 6px;

			& label {
				margin: 0 0.5rem;
				font-size: 0.8rem;
			}

			@media (max-width: 479px) {
				width: 100%;
			}
		`}</style>
	</div>
)

/// --------------------------------------------------------------------------------------------------------------------

export const InputContainer: FC = ({ children }) => (
	<div>
		{children}

		<style jsx>{`
			width: 100%;
			display: flex;
			flex-direction: row;
			gap: 12px;
			align-items: center;
			justify-content: space-between;

			@media (max-width: 479px) {
				flex-direction: column;
				align-items: flex-start;
				justify-content: flex-start;
			}
		`}</style>
	</div>
)
