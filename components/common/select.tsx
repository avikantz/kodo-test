/**
 * Select
 */

import { ChangeEventHandler, FC } from 'react'

import { COLORS } from 'styles'

/// --------------------------------------------------------------------------------------------------------------------

interface SelectInputProps {
	value?: string
	options?: { label: string; value: string }[]
	onChange?: ChangeEventHandler<HTMLSelectElement>
}

/// --------------------------------------------------------------------------------------------------------------------

export const SelectInput: FC<SelectInputProps> = ({ value, options, onChange }) => (
	<>
		<select placeholder="—" value={value} onChange={onChange}>
			{options.map(option => (
				<option key={option.value} value={option.value}>
					{option.label}
				</option>
			))}
		</select>
		<style jsx>{`
			width: 100%;
			padding: 0.25rem 0.5rem;
			font-size: 1rem;
			background: ${COLORS.BACKGROUND};
			border: 1px solid ${COLORS.BORDER};
			border-radius: 8px;
		`}</style>
	</>
)
