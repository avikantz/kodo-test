/**
 * Page footer
 */

import { FC } from 'react'

import { COLORS } from 'styles'

/// --------------------------------------------------------------------------------------------------------------------

export const Footer: FC = () => (
	<footer>
		<span>
			Created for{' '}
			<a href="https://kodo.in" rel="noopener noreferrer" target="_blank">
				Kodo
			</a>{' '}
			by{' '}
			<a href="https://avikantz.xyz" rel="noopener noreferrer" target="_blank">
				Avikant
			</a>
			.
		</span>

		<style jsx>{`
			footer {
				width: 100%;
				height: 96px;
				border-top: 1px solid ${COLORS.BORDER};
				display: flex;
				justify-content: center;
				align-items: center;
			}
		`}</style>
	</footer>
)
