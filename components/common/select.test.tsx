/**
 *
 */

import { render } from '@testing-library/react'

import { SelectInput } from './select'

const OPTIONS = [
	{ label: 'Apple', value: 'apple' },
	{ label: 'Banana', value: 'banana' }
]

describe('Select', () => {
	it('renders options', () => {
		const select = render(<SelectInput options={OPTIONS} />)
		expect(select).toBeDefined()
		expect(select.queryAllByRole('option')).toHaveLength(OPTIONS.length)
	})
})
