/**
 * Common page layout
 */

import { FC, ReactNode } from 'react'

import Head from 'next/head'

import { Footer } from 'components'

import { COLORS } from 'styles'

/// --------------------------------------------------------------------------------------------------------------------

interface LayoutProps {
	title?: string // Page title
	style?: string // Global style overrides
	children: ReactNode // Children, Layout is the main container, pages must use Layout to render their content
}

/// --------------------------------------------------------------------------------------------------------------------

export const Layout: FC<LayoutProps> = ({ title = 'Kodo', style = '', children }) => (
	<div className="main-container">
		{/** Page head content */}
		<Head>
			<title>{title}</title>
			<link rel="icon" href="/favicon.ico" />
		</Head>

		<noscript>Javascript must be enabled to run this app.</noscript>

		{/** Main body */}
		<main>{children}</main>

		{/** Footer, fixed to the bottom */}
		<Footer />

		{/** Style overrides */}
		<style jsx>{style}</style>

		{/** Global styles */}
		<style jsx global>{`
			html,
			body {
				padding: 0;
				margin: 0;
				font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
				color: ${COLORS.TEXT};
			}

			* {
				box-sizing: border-box;
			}

			.main-container {
				min-height: 100vh;
				padding: 0 0.5rem;
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}

			main {
				padding: 2rem 1rem;
				flex: 1;
				display: flex;
				flex-direction: column;
				gap: 2rem;
				align-items: start;
				width: 100%;
				max-width: 1200px;
			}

			a {
				color: ${COLORS.PRIMARY};
				text-decoration: none;
			}

			a:hover,
			a:focus,
			a:active {
				border-bottom: 1px dotted ${COLORS.PRIMARY};
			}

			@media (max-width: 479px) {
				main {
					padding: 1rem 0.5rem;
					gap: 1rem;
				}
			}
		`}</style>
	</div>
)

export default Layout
