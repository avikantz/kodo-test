/**
 * Item table
 */

import { FC } from 'react'

import { Item } from 'models'

import { COLORS } from 'styles'

/// --------------------------------------------------------------------------------------------------------------------

interface ItemRowProps {
	item: Item
}

interface ItemTableProps {
	items: Item[]
}

/// --------------------------------------------------------------------------------------------------------------------

const ItemRow: FC<ItemRowProps> = ({ item }) => (
	<tr>
		<td className="image">
			<img src={item.image} alt={item.name} />
		</td>
		<td className="name">
			<span>{item.name}</span>
			<small>Last Edited: {new Date(item.dateLastEdited).toDateString()}</small>
		</td>
		<td className="description">{item.description}</td>

		<style jsx>{`
			td {
				padding: 0.5rem;
				border-bottom: 1px solid ${COLORS.BORDER};
			}
			.image {
				min-width: 180px;
				max-width: 180px;
			}
			.image img {
				width: 100%;
				object-fit: cover;
				border-radius: 8px;
			}
			.name {
				min-width: 180px;
				max-width: 240px;
			}
			.name span {
				display: block;
			}
			.name small {
				font-size: 0.8rem;
				color: ${COLORS.SECONDARY};
			}
			.description {
				font-size: 0.8rem;
				color: ${COLORS.SECONDARY};
				min-width: 320px;
				max-width: 732px;
			}

			@media (max-width: 767px) {
				td {
					display: inline-block;
					padding: 0.25rem;
					border-bottom: 0;
				}
				td:last-child {
					border-bottom: 2px solid ${COLORS.BORDER};
					padding-bottom: 1rem;
					margin-bottom: 1rem;
				}
			}
		`}</style>
	</tr>
)

/// --------------------------------------------------------------------------------------------------------------------

export const ItemTable: FC<ItemTableProps> = ({ items }) => (
	<table>
		<thead>
			<tr>
				<th>Image</th>
				<th>Name</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			{items.map((item, index) => (
				<ItemRow key={'irow' + index} item={item} />
			))}
			{items.length == 0 &&
				Array(10)
					.fill(0)
					.map((z, i) => (
						<tr key={'brow' + i}>
							<td />
							<td />
							<td />
						</tr>
					))}
		</tbody>

		<style jsx>{`
			table {
				width: 100%;
				table-layout: auto;
				display: block;
				overflow-x: scroll;
				text-align: left;
			}

			table th {
				padding: 0.5rem;
				border-bottom: 1px solid ${COLORS.BORDER};
			}
			table td {
				padding: 77px 0.5rem;
				border-bottom: 1px solid ${COLORS.BORDER};
			}
			
			@media (max-width: 767px) {
				th {
					display: none;
				}
			}
		`}</style>
	</table>
)
