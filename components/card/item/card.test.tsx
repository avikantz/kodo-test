/**
 *
 */

import { render } from '@testing-library/react'

import { ItemCard } from './card'

import { MOCK_DATA } from 'data'

const ITEM = MOCK_DATA[Math.floor(Math.random() * MOCK_DATA.length)]

describe('Item card', () => {
	it('renders card', () => {
		const card = render(<ItemCard item={ITEM} />)
		expect(card.queryByRole('img').getAttribute('src')).toBe(ITEM.image)
		expect(card.queryByText(ITEM.name)).toBeDefined()
		expect(card.queryByText(ITEM.description)).toBeDefined()
		expect(card.queryByText(ITEM.dateLastEdited)).toBeDefined()
	})
})
