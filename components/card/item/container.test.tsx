/**
 *
 */

import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-17-updated'

import { render } from '@testing-library/react'
jest.mock('next/router', () => require('next-router-mock'))
configure({ adapter: new Adapter() })

import { ItemCardContainer } from './container'

import { MOCK_DATA } from 'data'

const si = Math.floor((Math.random() * MOCK_DATA.length) / 2)
const ITEMS = MOCK_DATA.slice(si, MOCK_DATA.length - si + Math.floor(Math.random() * si))

describe('Item card container', () => {
	it('renders container with items', () => {
		const container = render(<ItemCardContainer items={ITEMS} />)
		expect(container.queryAllByText(/Last Edited/i)).toHaveLength(Math.min(ITEMS.length, 6))
		expect(container.queryAllByRole('button').length).toBeGreaterThan(0)
	})

	it('paginates within container', () => {
		const container = mount(<ItemCardContainer items={ITEMS} />)
		const countItems = ITEMS.length
		expect(container.find('.card')).toHaveLength(Math.min(countItems, 6))

		const nextButton = container.findWhere(node => {
			return node.type() === 'button' && node.text() === '›'
		})
		let prevButton = container.findWhere(node => {
			return node.type() === 'button' && node.text() === '‹'
		})
		expect(prevButton.prop('disabled')).toBeTruthy()
		expect(nextButton).toBeDefined()
		
		nextButton.simulate('click')

		container.update()

		expect(container.find('.card')).toHaveLength(Math.min(countItems - 6, 6))

		prevButton = container.findWhere(node => {
			return node.type() === 'button' && node.text() === '‹'
		})
		expect(prevButton.prop('disabled')).toBeFalsy()
	})
})
