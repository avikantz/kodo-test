/**
 * Item card container
 */

import { FC, useEffect, useState } from 'react'

import { useRouter } from 'next/router'

import { ItemCard, ItemCardLoader } from './card'
import { Pagination } from 'components'

import { Item } from 'models'

import { ITEMS_PER_PAGE } from 'utils/constants'

/// --------------------------------------------------------------------------------------------------------------------

interface ItemCardContainerProps {
	items: Item[]
	page?: number
}

/// --------------------------------------------------------------------------------------------------------------------

export const ItemCardContainer: FC<ItemCardContainerProps> = ({ items: parentItems, page: defaultPage = 1 }) => {
	//
	const router = useRouter()
	//
	// Internally manage partial items for pagination
	const [items, setItems] = useState<Item[]>(parentItems)
	//
	// Hold the pagination information in state
	const [page, setPage] = useState(defaultPage)
	const [pageCount, setPageCount] = useState(1)
	//
	// Set items and pagination status on parent items change (filters/search)
	useEffect(() => {
		// Update pageCount and reset to default page/1
		setPage((defaultPage - 1) * ITEMS_PER_PAGE <= parentItems.length ? defaultPage : 1)
		setPageCount(Math.ceil(parentItems.length / ITEMS_PER_PAGE))
		// Set items
		setItems(parentItems.slice(0, ITEMS_PER_PAGE))
	}, [parentItems])
	//
	useEffect(() => {
		// Update query with page information
		router.replace(
			{
				pathname: '/',
				query: { ...router.query, page }
			},
			null,
			{ shallow: true }
		)
		// Slice items based on page
		setItems(parentItems.slice((page - 1) * ITEMS_PER_PAGE, Math.min(page * ITEMS_PER_PAGE, parentItems.length)))
	}, [page])
	//
	return (
		<>
			<div>
				{items.map((item, index) => (
					<ItemCard key={'item-' + index} item={item} />
				))}
				{items.length == 0 &&
					Array(6)
						.fill(0)
						.map((z, i) => <ItemCardLoader key={'loader' + i} />)}

				<style jsx>{`
					display: grid;
					grid-template-columns: repeat(3, 1fr);
					grid-gap: 1rem;
					align-content: center;
					justify-content: space-evenly;
					width: 100%;

					@media (max-width: 767px) {
						grid-template-columns: repeat(2, 1fr);
					}

					@media (max-width: 479px) {
						grid-gap: 0.8rem;
						grid-template-columns: 1fr;
					}
				`}</style>
			</div>
			<Pagination
				page={page}
				pageCount={pageCount}
				totalCount={parentItems.length}
				itemsPerPage={ITEMS_PER_PAGE}
				showStats
				onChange={page => setPage(page)}
			/>
		</>
	)
}
