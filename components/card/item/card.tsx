/**
 * Item card
 */

import { FC } from 'react'

import { Item } from 'models'

import { COLORS } from 'styles'

/// --------------------------------------------------------------------------------------------------------------------

interface ItemCardProps {
	item: Item
}

/// --------------------------------------------------------------------------------------------------------------------

export const ItemCard: FC<ItemCardProps> = ({ item }) => (
	<div className="card">
		{/** Card image */}
		<img src={item.image} alt={item.name} />

		{/** Card body */}
		<div className="body">
			<span className="title">{item.name}</span>
			<small className="description">{item.description.length > 140 ? `${item.description.substr(0, 140)}...` : item.description}</small>
		</div>

		{/** Card footer */}
		<div className="footer">
			<small>Last Edited: {new Date(item.dateLastEdited).toDateString()}</small>
		</div>

		<style jsx>{`
			.card {
				display: flex;
				flex-direction: column;
				border-radius: 8px;
				/* border: 1px solid ${COLORS.BORDER}; */
				transition: all 0.2s ease;
				box-shadow: 0 2px 2px 0 ${COLORS.SHADOW};
			}

			.card:hover {
				transform: scale(1.025);
				box-shadow: 0 2px 4px 0 ${COLORS.SHADOW_DARK};
				cursor: grab;
			}

			.card img {
				border-radius: 8px 8px 0 0;
				width: 100%;
				height: 200px;
				object-fit: cover;
			}

			.card .body {
				display: flex;
				flex-grow: 1;
				flex-direction: column;
				padding: 0.8rem;
				gap: 0.8rem;
			}

			.card .body .title {
				font-weight: bold;
			}

			.card .body .description {
				color: ${COLORS.SECONDARY};
			}

			.card .footer {
				font-size: 0.8rem;
				display: flex;
				flex-direction: row;
				padding: 0.8rem;
				gap: 0.4rem;
				border-top: 1px solid ${COLORS.BORDER};
			}

			@media (max-width: 767px) {
				.card .body {
					padding: 0.5rem;
					gap: 0.5rem;
				}

				.card .footer {
					flex-direction: column;
					padding: 0.5rem;
					gap: 0.2rem;
				}
			}
		`}</style>
	</div>
)

/// --------------------------------------------------------------------------------------------------------------------

export const ItemCardLoader: FC = ({}) => (
	<div>
		<style jsx>{`
			display: flex;
			border-radius: 8px;
			height: 342px;
			box-shadow: 0 2px 2px 0 ${COLORS.SHADOW};
		`}</style>
	</div>
)
