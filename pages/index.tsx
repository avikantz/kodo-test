/**
 *
 */

import { useEffect, useState } from 'react'

import { NextPage } from 'next'
import { useRouter } from 'next/router'

import { InputContainer, InputItem, Layout, SearchInput, SelectInput, Title } from 'components'
import { ItemCardContainer } from 'components/card'
import { ItemTable } from 'components/table'

import { fuzzySearch, strictSearch } from 'utils/search'

import { Item } from 'models'

import { MOCK_DATA } from 'data'

/// --------------------------------------------------------------------------------------------------------------------

interface HomePageProps {
	q?: string
	sort?: string
	page?: number
}

/// --------------------------------------------------------------------------------------------------------------------

export const Home: NextPage<HomePageProps> = ({ page: defaultPage = 1, q = '', sort: defaultSort = 'name' }) => {
	//
	const router = useRouter()
	//
	const [items, setItems] = useState<Item[]>([])
	//
	const [searchText, setSearchText] = useState<string>(q)
	const [sort, setSort] = useState(defaultSort)
	//
	// Handle search/sort
	useEffect(() => {
		let items: Item[] = [...MOCK_DATA]
		// Sorting by sort field if applicable
		if (sort) {
			if (sort === 'name') {
				items.sort((a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase()))
			} else if (sort === 'dateLastEdited') {
				items.sort((a, b) => new Date(b.dateLastEdited).getTime() - new Date(a.dateLastEdited).getTime())
			}
		}
		// Search if applicable
		if (searchText) {
			const st = searchText.replace(/\"/g, '')
			// Perform search
			if (fuzzySearch(searchText, '""')) {
				// Perform strict search if within quotes
				items = items.filter(item => strictSearch(item.name, st) || strictSearch(item.description, st))
			} else {
				// Perform fuzzy search if no quotes
				items = items.filter(item => fuzzySearch(item.name, st) || fuzzySearch(item.description, st))
			}
		} 
		// Update items
		setItems(items)
		// Update query
		router.replace(
			{
				pathname: '/',
				query: { ...router.query, sort, q: searchText }
			},
			null,
			{ shallow: true }
		)
	}, [searchText, sort])
	//
	return (
		<Layout>
			{/** Title */}
			<Title>
				F
				{Array(Math.max(2, Math.floor(items.length / 10)))
					.fill('e')
					.join('')}
				d
			</Title>
			{/** Search/sort container */}
			<InputContainer>
				<InputItem label="🔎 Search">
					<SearchInput value={searchText} onChange={({ target }) => setSearchText(target.value)} />
				</InputItem>
				<InputItem label="🎚 Sort by">
					<SelectInput
						value={sort}
						options={[
							{ label: '—', value: '' },
							{ label: 'Title', value: 'name' },
							{ label: 'Last Edited', value: 'dateLastEdited' }
						]}
						onChange={event => setSort(event.target.value)}
					/>
				</InputItem>
			</InputContainer>
			{/** Cards */}
			<ItemCardContainer items={items} page={defaultPage} />
			{/** Table */}
			<ItemTable items={items} />
		</Layout>
	)
}

Home.getInitialProps = async ({ query }) => {
	if (query) {
		const { page = 1, q = '', sort = '' } = query
		return { page: Number(page), q: Array.isArray(q) ? q[0] : q, sort: Array.isArray(sort) ? sort[0] : sort }
	}
	return { page: 1, q: '', sort: 'name' }
}

export default Home
