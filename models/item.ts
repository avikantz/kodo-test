/**
 * Item interface
 *
 * name
 * image
 * description
 * dateLastEdited
 */

export interface Item {
	name: string
	image: string
	description: string
	dateLastEdited: string
}
